﻿using System;

namespace Ejercicio_2
{
    class Persona
    {
        System.Random random = new System.Random();
        private string nombre = "";
        private int edad = 0;
        private int DNI;
        private char sexo = 'H';
        private float peso = 0;
        private float altura = 0;
        public Persona() 
        {
            this.generaDNI();
        }
        public Persona(string nombre,int edad,char sexo)
        {            
            this.generaDNI();
            this.nombre = nombre;
            this.edad = edad;
            this.sexo = sexo;

        }
        public Persona(string nombre, int edad,int DNI,char sexo,float peso,float altura) 
        {
            this.nombre = nombre;
            this.edad = edad;
            this.DNI = DNI;
            this.sexo = sexo;
            this.peso = peso;
            this.altura = altura;
        }
        public int calcularMci()
        {
            float calcu = (this.peso / (float)Math.Pow(this.altura, 2));
            if(calcu < 20)
            {
                return -1;
            }
            else if(calcu >= 20 && calcu <= 25)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        public bool esMayorDeEdad()
        {
            if (this.edad >= 18)
            {
                return true;
            }
            return false;
        }
        private void comprobarSexo()
        {
            if(this.sexo != 'H' && this.sexo != 'M')
            {
                this.sexo = 'H';
            }
        }
        private void generaDNI()
        {
            this.DNI = random.Next(10000000, 9999999);
        }
        public string Nombre
        {
            set
            {
                this.nombre = value;
            }
        }
        public int Edad
        {
            set
            {
                this.edad = value;
            }
        }
        public char Sexo
        {
            set
            {
                this.sexo = value;
            }
        }
        public float Peso
        {
            set
            {
                this.peso = value;
            }
        }
        public float Altura
        {
            set
            {
                this.altura = value;
            }
        }

        class Ejecutable
        {
            static void Main(string[] args)
            {
                Console.WriteLine("Ingresar un nombre: ");
                string nombre = Console.ReadLine();
                Console.WriteLine("Ingresar la edad: ");
                int edad = int.Parse(Console.ReadLine());
                Console.WriteLine("Ingresar el sexo ('H' para hombre y 'M' para mujer: ");
                char sexo = Console.ReadLine()[0];
                Console.WriteLine("Ingresar un valor para el peso: ");
                float peso = float.Parse(Console.ReadLine());
                Console.WriteLine("Ingrese un valor para la altura: ");
                float altura = float.Parse(Console.ReadLine());
                Console.WriteLine("Ingrese el DNI: ");
                int dni = int.Parse(Console.ReadLine());

                Persona[] obj = new Persona[3];
                obj[0] = new Persona(nombre, edad, dni, sexo, peso, altura);
                obj[1] = new Persona(nombre, edad, sexo);
                obj[2] = new Persona();
                obj[2].Nombre = "Martin";
                obj[2].Edad = 20;
                obj[2].Sexo = 'H';
                obj[2].Altura = 184.2F;
                obj[2].Peso = 65.8F;

                foreach (Persona p in obj)
                {
                    switch (p.calcularMci())
                    {
                        case -1:
                            Console.WriteLine("Esta por debajo de su peso ideal");
                            break;
                        case 0:
                            Console.WriteLine("Esta en su peso ideal");
                            break;
                        case 1:
                            Console.WriteLine("Tiene sobrepeso");
                            break;
                    }
                }
            }
        }
    }
}
