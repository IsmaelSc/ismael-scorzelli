﻿using System;

namespace Ejercicio_6
{
    class Libro
    {
        private int    iSBN;
        private string titulo;
        private string autor;
        private int    numero_de_paginas;

        public Libro(int iSBN, string titulo, string autor, int numero_de_paginas)
        {
            this.iSBN = iSBN;
            this.titulo = titulo;
            this.autor = autor;
            this.numero_de_paginas = numero_de_paginas;
        }
        public int ISBN
        {
            get
            {
                return this.iSBN;
            }
            set
            {
                this.iSBN = value;
            }
        }
        public string Titulo
        {
            get
            {
                return this.titulo;
            }
            set
            {
                this.titulo = value;
            }
        }
        public string Autor
        {
            get
            {
                return this.autor;
            }
            set
            {
                this.autor = value;
            }
        }
        public int Numero_de_paginas
        {
            get
            {
                return this.numero_de_paginas;
            }
            set
            {
                this.numero_de_paginas = value;
            }
        }

        public string toString()
        {
            return "El libro " + titulo + " con ISBN " + ISBN + "" + " creado por el autor " + autor + " tiene " + numero_de_paginas + " páginas";
        }
    }
    class Ejecutable
    {
        static void Main(string[] args)
        {
                
            Libro libro1 = new Libro(400123, "Quién se ha llevado mi queso", "Spencer Johnson", 106);
            Libro libro2 = new Libro(124357, "Mentalidad de tiburón", "Manuel Sotomayor", 192);

            if (libro1.Numero_de_paginas > libro2.Numero_de_paginas)
            {
                Console.WriteLine("El libro " + libro1.Titulo + " tiene mas paginas");
            }
            else
            {
                Console.WriteLine("El libro " + libro2.Titulo + "tiene mas paginas");
            }
        }
    }
    
}
