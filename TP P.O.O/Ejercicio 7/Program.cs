﻿using System;

namespace Ejercicio_7
{
    class Raices
    {
        private double a;
        private double b;
        private double c;

        public Raices(double a, double b, double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        public void obtenerRaices()
        {
            double solucion = Math.Sqrt(this.b * this.b - 4 * (this.a) * (this.c));
            
            if (solucion > 0)
            {
                double resul_posi = (-1*(this.b) + (solucion)) / 2 * this.a;
                double resul_nega = (-1*(this.b) - (solucion)) / 2 * this.a;

                Console.WriteLine("la solucion 1 es:" + resul_posi + " y la soluciones 2 es:" + resul_nega);
            }
        }
        public void obtenerRaiz()
        {
            double solucion = Math.Sqrt(this.b * this.b - 4 * (this.a) * (this.c));
            
            if (solucion == 0)
            {
                Console.WriteLine("La única raíz es " + (-1*(this.b) / 2 * this.a));
            }
        }
        public double getDiscriminante()
        {
            return (this.b * this.b) - 4 * this.a * this.c;
        }
        public bool tieneRaices()
        {
            double solucion = this.getDiscriminante();

            return (solucion >= 0);
        }
        public bool tieneRaiz()
        {
            double solucion = this.getDiscriminante();
            return (solucion == 0);
        }
        public void calcular()
        {
            this.obtenerRaices();

            if (this.tieneRaiz())
            {
                this.obtenerRaiz();
            }
            else if(this.tieneRaices())
            {
                this.obtenerRaices();
            }
            else
            {
                Console.WriteLine("No existe solucion");
            }

            
        }
    }
    class Ejecutable
    {
        static void Main(string[] args)
        {
            Raices raiz = new Raices(1, 25, 5);
            raiz.obtenerRaices();
        }
    }
}
