﻿using System;
using System.Collections.Generic;
namespace Ejercicio_12_bis
{
    abstract class Empleado
    {
        protected string nombre;
        protected int edad;
        protected float salario;
        protected const float Plus = 300;

        public Empleado(string nombre, int edad, float salario)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.salario = salario;
        }
        public string Nombre
        {
            get
            {
                return this.nombre;
            }
            set
            {
                this.nombre = value;
            }
        }
        public int Edad
        {
            get
            {
                return this.edad;
            }
            set
            {
                this.edad = value;
            }
        }
        public float Salario
        {
            get
            {
                return this.salario;
            }
            set
            {
                this.salario = value;
            }
        }
        public string toString()
        {
            return String.Format("El empleado {0} tiene {1} años, su salario es {2}", this.nombre, this.edad, this.salario);
        }
        public abstract bool plus();
    }
    class Repartidor : Empleado
    {
        private string zona;
        public Repartidor(string nombre, int edad, float salario, string zona) : base(nombre, edad, salario)
        {
            this.zona = zona;
        }
        public override bool plus()
        {
            if (this.edad < 25 && this.zona == "zona 3")
            {
                this.salario += Plus;
                return true;
            }
            return false;
        }
        public new string toString()
        {
            return String.Format("{0} y su zona es {1}", base.toString(), this.zona);
        }

    }
    class Comercial : Empleado
    {
        private double comision;
        public Comercial(string nombre, int edad, float salario, double comision) : base(nombre, edad, salario)
        {
            this.comision = comision;
        }
        public override bool plus()
        {
            if (this.edad > 30 && this.salario > 200)
            {
                this.salario += Plus;
                return true;
            }
            return false;
        }
        public new string toString()
        {
            return String.Format("{0} y su comision es {1}", base.toString(), this.comision);
        }
    }
    class Ejecutable
    {
        static void Main(string[] args)
        {

            Comercial c = new Comercial("Franchesco", 25, 250,20);
            Repartidor e = new Repartidor("Pablo", 30, 300, "zona 3");

            c.plus();
            e.plus();

            Console.WriteLine(c);
            Console.WriteLine(e);
        }
    }
}