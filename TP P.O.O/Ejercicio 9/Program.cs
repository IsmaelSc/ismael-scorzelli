﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;


namespace Ejercicio_9
{
    class Cine
    {
        private List<List<Asiento>> asientos = new List<List<Asiento>>();
        private Pelicula pelicula;
        private float precio_entrada;
        public Cine(float precio, Pelicula pelicula)
        {
            this.precio_entrada = precio;
            this.pelicula = pelicula;
        }
        public List<List<Asiento>> Asientos
        {
            get
            {
                return this.asientos;
            }
        }
        public float Precio
        {
            get
            {
                return this.precio_entrada;
            }
        }
        public Pelicula Pelicula
        {
            get
            {
                return this.pelicula;
            }
        }
        public void rellenarButaca(int filas, int columnas)
        {
            int columnas_t = (columnas > 26) ? 26 : columnas;
            for (int i = 0; i < filas; i++)
            {
                List<Asiento> fila_temp = new List<Asiento>();

                for (int z = 0; z < columnas_t; z++)
                {
                    fila_temp.Add(new Asiento((char)(z + 65), i + 1));
                }
                this.asientos.Add(fila_temp);
            }
        }
        public bool hayLugar()
        {
            foreach (List<Asiento> fila in asientos)
            {
                foreach (Asiento p in fila)
                {
                    if (p.Espectador == null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public bool hayLugarButaca(int fila, char col)
        {
            int col_index = letra_col_to_index(col);
            if (col_index == -1)
            {
                return false;
            }
            if (fila >= asientos.Count)
            {
                return false;
            }
            return (asientos[fila][col_index].Espectador == null);
        }
        public List<Asiento> ButacasDisponibles()
        {
            List<Asiento> temp = new List<Asiento>();
            foreach(List<Asiento> fila in this.asientos)
            {
                foreach(Asiento a in fila)
                {
                    if (!a.ocupado())
                    {
                        temp.Add(a);
                    }
                }
            }
            return temp;
        }

        public bool sePuedeSentar(Espectador e)
        {
            return (e.tieneEdad(this.pelicula.Edad_minima) && e.tieneDinero(this.precio_entrada));
        }
        public void sentar(int fila, char col, Espectador e)
        {
            this.asientos[fila][letra_col_to_index(col)].Espectador = e;
        }
        public Asiento Asiento(int fila, char col)
        {
            return this.asientos[fila][this.letra_col_to_index(col)];
        }
        private int letra_col_to_index(char col)
        {
            int col_index = -1;
            if (((int)col >= 65 && (int)col <= 90) || (int)col >= 97 && (int)col <= 122)
            {
                if ((int)col <= 90)
                {
                    col_index = (int)col - 65;
                }
                else
                {
                    col_index = (int)col - 97;
                }
            }
            return col_index;
        }
        public int Filas()
        {
            return asientos.Count;
        }
        public string toString()
        {
            string text = String.Format("Se esta dando la pelicula: {0} \n Hay {1} filas y {2} columnas. \n Los asientos son: \n", this.pelicula.toString(), this.Filas(), this.asientos[0].Count);
            foreach (List<Asiento> asi in this.asientos)
            {
                foreach (Asiento a in asi)
                {
                    text += a.toString();
                }
            }
            return text;
        }
    }
    class Espectador
    {
        private string nombre;
        private int edad;
        private double dinero;

        public Espectador(string nombre, int edad, double dinero)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.dinero = dinero;
        }

        public string Nombre
        {
            get
            {
                return this.nombre;
            }
            set
            {
                this.nombre = value;
            }
        }
        public int Edad
        {
            get
            {
                return this.edad;
            }
            set
            {
                this.edad = value;
            }
        }
        public double Dinero
        {
            get
            {
                return this.dinero;
            }
            set
            {
                this.dinero = value;
            }
        }
        public bool tieneEdad(int edad_minima)
        {
            if (edad >= edad_minima)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool tieneDinero(float precio_entrada)
        {
            if (dinero >= precio_entrada)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    class Pelicula
    {
        private String titulo;
        private int duracion;
        private int edad_minima;
        private String director;
        public Pelicula(string titulo, int duracion, int edad_minima, string director)
        {
            this.titulo = titulo;
            this.duracion = duracion;
            this.edad_minima = edad_minima;
            this.director = director;
        }
        public string Titulo
        {
            get
            {
                return this.titulo;
            }
            set
            {
                this.titulo = value;
            }
        }
        public int Duracion
        {
            get
            {
                return this.duracion;
            }
            set
            {
                this.duracion = value;
            }

        }
        public int Edad_minima
        {
            get
            {
                return this.edad_minima;
            }
            set
            {
                this.edad_minima = value;
            }
        }
        public string Director
        {
            get
            {
                return this.director;
            }
            set
            {
                this.director = value;
            }
        }
        public string toString()
        {
            return String.Format("{0} del director {1}, con una duración de {2} minutos y la edad mínima es de {3} años.", this.titulo, this.director, this.duracion, this.edad_minima);
        }
    }
    class Asiento
    {
        private char letra;
        private int fila;
        private Espectador espectador;
        public Asiento(char letra, int fila)
        {
            this.letra = letra;
            this.fila = fila;
        }
        public char Letra
        {
            get
            {
                return this.letra;
            }
            set
            {
                this.letra = value;
            }
        }
        public int Fila
        {
            get
            {
                return this.fila;
            }
            set
            {
                this.fila = value;
            }

        }
        public Espectador Espectador
        {
            get
            {
                return this.espectador;
            }
            set
            {
                this.espectador = value;
            }
        }
        public bool ocupado()
        {
            return espectador != null;
        }
        public string toString()
        {
            if (ocupado())
            {
                return String.Format("Asiento: {0} y {1}", fila, letra);
            }
            else
            {
                return String.Format("Asiento: {0} y {1} está vacío", fila, letra);
            }
        }
    }
    class Ejecutable
    {
        static void Main(string[] args)
        {
            Pelicula pelicula = new Pelicula("Harry Potter", 125, 13, "David Yates");
            Console.WriteLine("Ingresa la cantidad de filas:");
            int filas = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingresa un numero entre el 1 y el 26 para la cantidad de columnas:");
            int cols = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingresa un numero para el precio de la entrada:");
            int precio = int.Parse(Console.ReadLine());
            Cine cine = new Cine(precio, pelicula);
            Console.WriteLine("Ingrese un numero para la cantidad de espectadores:");
            int c_espectadores = int.Parse(Console.ReadLine());
            List<Espectador> espectadores = new List<Espectador>();
            Random rdn = new Random(DateTime.UtcNow.Millisecond);
            for (int i = 0; i < c_espectadores; i++)
            {
                espectadores.Add(new Espectador(i.ToString(), rdn.Next(5, 35), rdn.Next(0, 1000)));
                if (cine.hayLugar())
                {
                    if (cine.sePuedeSentar(espectadores.Last()))
                    {
                        List<Asiento> l = cine.ButacasDisponibles();
                        Asiento a = l[rdn.Next(0, l.Count)];
                        cine.sentar(a.Fila, a.Letra, espectadores.Last());
                    }
                }
            }
            Console.WriteLine(cine.toString());
        }

    }
}