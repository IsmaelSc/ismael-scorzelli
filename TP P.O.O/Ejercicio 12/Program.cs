﻿using System;
using System.Collections.Generic;
namespace Ejercicio_12
{
    class Revolver
    {
        private int posActual;
        private int posBala;

        public Revolver()
        {
            Random rdn = new Random(DateTime.UtcNow.Millisecond);
            this.posActual = rdn.Next(0, 5);
            this.posBala = rdn.Next(0, 5);
        }
        public bool disparar()
        {
            if (posActual == posBala)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void siguienteBala()
        {
            if (posActual < 5)
            {
                this.posActual = (posActual++);
            }
            else
            {
                this.posActual = 0;
            }
        }
        public void toString()
        {
            Console.WriteLine("La posicion actual es {0} y la bala esta en la posicion {1}", this.posActual, this.posBala);
        }
    }
    class Jugador
    {
        private int id = 1;
        private string nombre;
        private bool vivo = true;
        public Jugador(int id) 
        {
            this.id = id;
            this.nombre = String.Format("Jugador {0}", id);
        }
        public bool Vivo
        {
            get
            {
                return this.vivo;
            }
        }
        public bool disparar(Revolver r)
        {
            if (r.disparar())
            {
                return this.vivo = false;
            }
            else
            {
                return this.vivo = true;
            }
        }
    }
    class Juego
    {
        List<Jugador> jugadores;
        Revolver revolver = new Revolver();
        public Juego(int cant_jugadores)
        {
            int j = cant_jugadores % 7;
            for (int z = 0; z < j; z++)
            {
                jugadores.Add(new Jugador(z + 1));
            }
        }
        public bool finJuego()
        {
            foreach (Jugador j in this.jugadores)
            {
                if (!j.Vivo)
                {
                    return true;
                }
            }
            return false;
        }
        public void ronda()
        {
            foreach (Jugador j in this.jugadores)
            {
                if (j.disparar(revolver))
                {
                    break;
                }
            }
        }
    }
    class Ejecutable
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese un numero entre el 1 y el 6 para la cantidad de jugadores");
            int cant_j = int.Parse(Console.ReadLine());
            Juego juego = new Juego(cant_j);
            while (!juego.finJuego())
            {
                juego.ronda();
            }
        }
    }
}
