﻿using System;
using System.Collections.Generic;

namespace Ejercicio_10
{
    class Carta
    {
        private int numero;
        private string palo;

        public Carta(int numero, string palo)
        {
            this.numero = numero;
            this.palo = palo;
        }
    }
    class Baraja
    {
        private List<Carta> cartas = new List<Carta>();
        private List<Carta> cartas_dadas = new List<Carta>();

        public Baraja()
        {
            for (int i = 0; i < 10; i++)
            {
                cartas.Add(new Carta(i,"espada"));
            }
            for (int i = 0; i < 10; i++)
            {
                cartas.Add(new Carta(i, "copa"));
            }
            for (int i = 0; i < 10; i++)
            {
                cartas.Add(new Carta(i, "basto"));
            }
            for (int i = 0; i < 10; i++)
            {
                cartas.Add(new Carta(i, "oro"));
            }
        }
        public void barajar()
        {
            int num_car = cartas.Count; 

            List<Carta> car = new List<Carta>();

            for (int i = 0; i < num_car; i++)
            {            
                Random rnd = new Random();
                int ale = rnd.Next(0,(cartas.Count-1));
                car.Add(cartas[ale]);
                cartas.RemoveAt(ale);
            }            
        }
        public Carta SiguienteCarta()
        {
            if(cartas.Count < 40)
            {
               return cartas[0];
            }
            else
            {
                return null;      
            }
  
        }
        public int cartasDisponibles()
        {
            int num_car = cartas.Count;
            return num_car;
        }
        public List<Carta> darCartas(int cartas_pedidas)
        {
            List<Carta> cartapedidas = new List<Carta>();

            if (cartas_pedidas <= cartas.Count)
            {
                for (int i=0; i < cartas.Count; i++)
                {
                    cartapedidas.Add(cartas[i]);
                    cartas_dadas.Add(cartas[i]);
                    cartas.RemoveAt(i);
                }
                return cartapedidas;
            }
            else
            {
                return null;
            }
        }
        public List<Carta> cartasMonton()
        {
            if (cartas_dadas.Count > 0)
            {
                return cartas_dadas;
            }
            else
            {
                return null;
            }
            
        }
        public List<Carta> mostrarBaraja()
        {
            if (cartas.Count > 0)
            {
                return cartas;
            }
            else
            {
                return null;
            }            
        }
    }
}
