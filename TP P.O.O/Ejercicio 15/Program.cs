﻿using System;
using System.Collections.Generic;
namespace Ejercicio_15
{
    class Contactos
    {
        protected string nombre;
        protected int telefono;

        public Contactos(string nombre, int telefono)
        {
            this.nombre = nombre;
            this.telefono = telefono;
        }
        public string getNombre
        {
            get
            {
                return this.nombre;
            }
        }
        public int getTelefono
        {
            get
            {
                return this.telefono;
            }
        }
        public string toString()
        {
            return String.Format("Nombre: {0} Telefono: {1}", this.nombre, this.telefono);
        }
    }
    class Agenda
    {
        private List<Contactos> contactos = new List<Contactos>();
        private int tamaño = 10;
        public Agenda(int tamaño)
        {
            this.tamaño = tamaño;
        }
        public bool añadirContacto(Contactos c)
        {
            if (this.contactos.Count == this.tamaño)
            {
                Console.WriteLine("La lista de contacto esta llena");
                return false;
            }
            foreach (Contactos i in this.contactos)
            {
                if (c.getNombre == i.getNombre)
                {
                    Console.WriteLine("No se pueden meter contactos que existan");
                    return false;
                }
            }
            this.contactos.Add(c);
            return true;
        }
        public bool existeContacto(Contactos c)
        {
            foreach (Contactos i in this.contactos)
            {
                if (c.getNombre == i.getNombre)
                {
                    return false;
                }
            }
            return true;
        }
        public void listarContactos(Contactos c)
        {
            foreach (Contactos i in this.contactos)
            {
                Console.WriteLine(i.toString());
            }
        }
        public int buscaContacto(String nombre)
        {
            foreach (Contactos i in this.contactos)
            {
                if (nombre == i.getNombre)
                {
                    return i.getTelefono;
                }
            }
            return -1;
        }
        public bool eliminarContacto(Contactos c)
        {
            foreach (Contactos i in this.contactos)
            {
                if (c.getNombre == i.getNombre)
                {
                    if (this.contactos.Remove(c))
                    {
                        Console.WriteLine("Se ha eliminado el contacto");
                        return true;
                    }

                }
            }
            Console.WriteLine("No se ha podido eliminar el contacto");
            return false;
        }
        public bool agendaLlena()
        {
            if (this.contactos.Count == this.tamaño)
            {
                return true;
            }
            return false;
        }
        public int huecosLibres()
        {
            if (this.contactos.Count < this.tamaño)
            {
                return this.contactos.Count - this.tamaño;
            }
            return -1;
        }
    }
    class Ejecutable
    {
        static Agenda agenda;
        static void Main(string[] args)
        {
            Console.WriteLine("1. Añada un contacto");
            Console.WriteLine("2. Listar contactos");
            Console.WriteLine("3. Buscar contacto");
            Console.WriteLine("4. Elimine un contacto");
            Console.WriteLine("5. Huecos libres");
            Console.Write("seleccione una opcion: ");
            ConsoleKeyInfo input = Console.ReadKey();
            ConsoleKeyInfo tecla = Console.ReadKey();
            switch (tecla.KeyChar)
            {
                case '1':
                    Console.Clear();
                    añadir_contacto();
                    break;
                case '2':
                    
                    break;
                case '3':
                    Console.Clear();
                    
                    break;
                case '4':
                    Console.Clear();

                    break;
                case '5':
                    Console.Clear();

                    break;
                case '6':
                   // salir = true;
                    break;

                default:
                    break;
            }
            static void añadir_contacto()
            {
                Console.WriteLine("Ingrese el nombre para el nuevo contacto");
                string nom = Console.ReadLine();
                bool numero = false;
                int num = 0;
                while (!numero)
                {
                    Console.WriteLine("Ingrese un numero para el contacto");
                    numero = int.TryParse(Console.ReadLine(), out num);
                }
                agenda.añadirContacto(new Contactos(nom, num));
                Console.WriteLine("Presione cualquier tecla para continuar");
                Console.ReadKey();
            }
        }
    }
}