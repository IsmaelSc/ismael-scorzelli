﻿using System;

namespace Ejercicio_5
{

    interface Entregable
    {
        void entregar();
        void devolver();
        bool isEntregado();
        int compareTo(Object a);
    }

    class Serie : Entregable
    {
        private string titulo;
        private int    numero_de_temporadas = 3;
        private bool   entregado = false;
        private string genero;
        private string creador;

        public Serie() {}
        public Serie(string titulo, string creador)
        {
            this.titulo = titulo;
            this.creador = creador;
        }
        public Serie(string titulo, int numero_de_temporadas, string genero, string creador)
        {
            this.titulo               = titulo;
            this.numero_de_temporadas = numero_de_temporadas;
            this.creador              = creador;
        }

        public string Titulo
        {
            get
            {
                return this.titulo;
            }
            set
            {
                this.titulo = value;
            }
        }
        public int Numero_de_temporadas
        {
            get
            {
                return this.numero_de_temporadas;
            }
            set
            {
                this.numero_de_temporadas = value;
            }
        }
        public string Genero
        {
            get
            {
                return this.genero;
            }
            set
            {
                this.genero = value;
            }
        }
        public string Creador
        {
            get
            {
                return this.creador;
            }
            set
            {
                this.creador = value;
            }
        }

        public int compareTo(object a)
        {
            if (a is Videojuego)
            {
                Videojuego cast = (Videojuego)a;
                if (this.numero_de_temporadas > cast.Horas_estimadas)
                {
                    return 1;
                }
                return -1;
            }
            else if (a is Serie)
            {
                Serie cast = (Serie)a;
                if (this.numero_de_temporadas > cast.Numero_de_temporadas)
                {
                    return 1;
                }
                return -1;
            }
            else
            {
                return 0;
            }
        }

        public void devolver()
        {
            this.entregado = false;
        }

        public void entregar()
        {
            this.entregado = true;
        }

        public bool isEntregado()
        {
            return this.entregado;
        }
    }
    class Videojuego : Entregable
    {
        private string titulo;
        private int    horas_estimadas = 10;
        private bool   entregado = false;
        private string genero;
        private string compañia;

        public Videojuego() { }
        public Videojuego(string titulo, int horas_estimadas)
        {
            this.titulo = titulo;
            this.horas_estimadas = horas_estimadas;
        }
        public Videojuego(string titulo, int horas_estimadas, string genero, string compañia)
        {
            this.titulo = titulo;
            this.horas_estimadas = horas_estimadas;
            this.genero = genero;
            this.compañia = compañia;
        }
        public string Titulo
        {
            get
            {
                return this.titulo;
            }
            set
            {
                this.titulo = value;
            }
        }
        public int Horas_estimadas
        {
            get
            {
                return this.horas_estimadas;
            }
            set
            {
                this.horas_estimadas = value;
            }
        }
        public string Genero
        {
            get
            {
                return this.genero;
            }
            set
            {
                this.genero = value;
            }
        }
        public string Compañia
        {
            get
            {
                return this.compañia;
            }
            set
            {
                this.compañia = value;
            }
        }

        public int compareTo(object a)
        {
            if (a is Videojuego)
            {
                Videojuego cast = (Videojuego)a;
                if (this.horas_estimadas > cast.horas_estimadas )
                {
                    return 1;            
                }
                return -1;
            }
            else if(a is Serie)
            {
                Serie cast = (Serie)a;
                if (this.horas_estimadas > cast.Numero_de_temporadas)
                {
                    return 1;
                }
                return -1;
            }
            else
            {
                return 0;
            }
        }

        public void devolver()
        {
            this.entregado = false;
        }

        public void entregar()
        {
            this.entregado = true;
        }

        public bool isEntregado()
        {
            return this.entregado;
        }
    }

    class Ejecutable
    {
        static void Main(string[] args)
        {
            Serie[] series = new Serie[5];
            for (byte i = 0; i < 5; i++)
                series[i] = new Serie();

            series[1].entregar();
            series[2].entregar();
            series[3].entregar();

            Videojuego[] videojuegos = new Videojuego[5];
            for (byte i = 0; i < 5; i++)
                videojuegos[i] = new Videojuego();

            videojuegos[1].entregar();
            videojuegos[2].entregar();
            videojuegos[3].entregar();

            int contador = 0;
            foreach (Serie serie in series)
            {
                if (serie.isEntregado())
                {
                    contador ++;
                    serie.devolver();
                }  
            }

            int contador2 = 0;
            foreach (Videojuego videojuego in videojuegos)
            {
                if (videojuego.isEntregado())
                {
                    contador2++;
                    videojuego.devolver();
                }
            }
            Console.WriteLine("Hay entregados {0} series",contador);
            Console.WriteLine("Hay entregados {0} videojuegos", contador2);
        }
    }
}

