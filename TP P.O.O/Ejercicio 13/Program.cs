﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;

namespace Ejercicio_13
{
    class Producto
    {
        protected string nombre;
        protected float precio;

        public Producto(string nombre, float precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }
        public string getNombre
        {
            get
            {
                return this.nombre;
            }
        }
        public string setNombre
        {
            set
            {
                this.nombre = value;
            }
        }
        public float getPrecio
        {
            get
            {
                return this.precio;
            }
        }
        public float setPrecio
        {
            set
            {
                this.precio = value;
            }
        }
        public double calcular(int cantidad)
        {

            return precio * cantidad;
        }
        public string toString()
        {
            return String.Format("El nombre es {0} y el precio es de {1}", this.nombre, this.precio);
        }
    }
    class Perecedero : Producto
    {
        private int diasCaducar;
        public Perecedero(string nombre, float precio, int diasCaducar) : base(nombre, precio)
        {
            this.diasCaducar = diasCaducar;
        }
        public String toString(Producto r)
        {
            return r.toString() + String.Format("dias a caducar= {0}", this.diasCaducar);
        }
        public int getDiaCaducar
        {
            get
            {
                return this.diasCaducar;
            }
        }
        public int setDiaCaducar
        {
            set
            {
                this.diasCaducar = value;
            }
        }
        public new double calcular(int cantidad)
        {

            base.calcular(cantidad);
            switch (this.diasCaducar)
            {
                case 1:
                    this.precio /= 4;
                    break;
                case 2:
                    this.precio /= 3;
                    break;
                case 3:
                    this.precio /= 2;
                    break;
            }
            return precio;
        }
    }
    class NoPerecedero : Producto
    {
        private string tipo;
        public NoPerecedero(string nombre, float precio, string tipo) : base(nombre, precio)
        {
            this.tipo = tipo;
        }
        public string toString(Producto r)
        {
            return r.toString() + String.Format(" tipo= {0}", this.tipo);
        }
        public string getTipo
        {
            get
            {
                return this.tipo;
            }
        }
        public string setTipo
        {
            set
            {
                this.tipo = value;
            }
        }
    }
    class Ejecutable
    {
        static void Main(string[] args)
        {
            List<Producto> productos = new List<Producto>();
            productos.Add(new Perecedero("azucar", 10, 6));
            productos.Add(new Perecedero("harina", 20, 7));
            productos.Add(new Perecedero("pan", 50, 6));
            productos.Add(new NoPerecedero("aceite", 30, "vegetal"));
            productos.Add(new NoPerecedero("sardina", 40, "pescado"));

            foreach (Producto p in productos)
            {
                if (p is Perecedero)
                {
                    Perecedero pp = (Perecedero)p;
                    pp.calcular(5);
                    Console.WriteLine(pp.toString());
                }
                else if (p is NoPerecedero)
                {
                    NoPerecedero np = (NoPerecedero)p;
                    np.calcular(5);
                    Console.WriteLine(np.toString());
                }
            }
        }
    }
}
