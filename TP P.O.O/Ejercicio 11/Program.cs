﻿using System;

namespace Ejercicio_11
{
    class Program
    {
        class Jugador
        {
            private string nombre;
            private float dinero;
            private int porras_ganadas;
            private string[] resultados;
            public Jugador(string nombre)
            {
                this.nombre = nombre;
                this.dinero = 10;
                this.resultados = new string[15];
                this.reiniciar_resultados();
            }
            public void reiniciar_resultados()
            {
                for (int z = 0; z < 10; z++)
                {
                    this.resultados[z] = "";
                }
            }
            public bool puedePagar(float costo_porra)
            {
                return this.dinero >= costo_porra;
            }
            public void Bote(float costo_porra)
            {
                this.dinero -= costo_porra;
                Console.WriteLine("El jugador: {0} sumo {1} pesos al bote y le quedan {2} pesos", this.nombre, costo_porra, this.dinero);
            }
            public void ganarBote(float bote)
            {
                this.dinero += bote;
                Console.WriteLine("El jugador: {0} ha ganado {1} pesos al bote y le quedan {2} pesos", this.nombre, bote, this.dinero);
            }
            public void generarResultados()
            {
                Random rdn = new Random(DateTime.UtcNow.Millisecond);
                for (int i = 0; i < resultados.Length; i++)
                {
                    int local = rdn.Next(0, 15);
                    int visitante = rdn.Next(0, 15);
                    resultados[i] = String.Format("{0} - {1}", local, visitante);
                    Console.WriteLine("El jugador {0} eligio el resultado {1}", this.nombre, resultados[i]);
                }
            }
            public bool Acertado(string[] lista)
            {
                for (int i = 0; i < this.resultados.Length; i++)
                {
                    if (this.resultados[i] != lista[i])
                    {
                        return false;
                    }
                }
                return true;
            }
            public string toString()
            {
                return String.Format("El jugador {0} tiene {1} pesos y gano {2} porras", this.nombre, this.dinero, this.porras_ganadas);
            }
        }
        class Porra
        {
            private float bote = 0;
            Jugador[] jugadores;
            public Porra(Jugador[] jugadores)
            {
                this.jugadores = jugadores;
            }
            public void vaciarBote()
            {
                this.bote = 0;
            }
            public void aumentarBote(float dinero)
            {
                this.bote += dinero;
            }
            public void jornadas()
            {
                Resultado resultado = new Resultado();
                string[] partidos;
                for (int q = 0; q < 15; q++)
                {
                    Console.WriteLine("Jornada {0}", q + 1);
                    for (int k = 0; k < jugadores.Length; k++)
                    {
                        if (jugadores[k].puedePagar(1))
                        {
                            jugadores[k].Bote(1);
                            jugadores[k].generarResultados();
                            this.aumentarBote(1);
                        }
                        else
                        {
                            jugadores[k].reiniciar_resultados();
                        }
                    }
                }
                resultado.generarResultados();
                partidos = resultado.getPartidos;
                for (int h = 0; h < jugadores.Length; h++)
                {
                    if (jugadores[h].Acertado(partidos))
                    {
                        jugadores[h].ganarBote(this.bote);
                        this.vaciarBote();
                    }
                }
                for (int x = 0; x < jugadores.Length; x++)
                {
                    Console.WriteLine(jugadores[x].toString());
                }
            }
            public string toString()
            {
                return String.Format("En la porra hay de bote" + bote + " euro/s");
            }
        }
        class Resultado
        {
            private string[] partidos;
            public Resultado()
            {
                this.partidos = new string[10];
            }
            public void generarResultados()
            {
                Random rdn = new Random(DateTime.UtcNow.Millisecond);
                for (int g = 0; g < partidos.Length; g++)
                {
                    int local = rdn.Next(0, 10);
                    int visitante = rdn.Next(0, 10);
                    partidos[g] = String.Format("{0} - {1}", local, visitante);
                    Console.WriteLine("El partido {0} ha generado el resultado {1}", (g + 1), partidos[g]);
                }
            }
            public string[] getPartidos
            {
                get
                {
                    return this.partidos;
                }
            }
        }
        class Ejecutable
        {
            static void Main(string[] args)
            {
                Jugador[] jugadores = new Jugador[5];
                jugadores[0] = new Jugador("Pedro");
                jugadores[1] = new Jugador("Juan");
                jugadores[2] = new Jugador("Dolores");
                jugadores[3] = new Jugador("Fernando");
                jugadores[4] = new Jugador("Luis");
                Porra p = new Porra(jugadores);
                p.jornadas();
            }
        }
    }
}
